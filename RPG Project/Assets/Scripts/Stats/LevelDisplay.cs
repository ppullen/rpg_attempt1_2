﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Stats
{ 
    public class LevelDisplay : MonoBehaviour
    {
        BaseStats baseStats;
        private void Awake()
        {
            baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
        }

        private void Update()
        {
            /*
             * '{0:0}' translates as, take the first thing on the right with a precsion of zero '{0:0.0}' would be one decimal place
             */
            GetComponent<Text>().text = String.Format("{0:0}", baseStats.GetLevel());
        }
    }
}

