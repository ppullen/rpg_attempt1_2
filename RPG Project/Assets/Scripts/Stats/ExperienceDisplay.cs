﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Stats
{ 
    public class ExperienceDisplay : MonoBehaviour
    {
        Experience experience;
        private void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
        }

        private void Update()
        {
            /*
             * '{0:0}' translates as, take the first thing on the right with a precsion of zero '{0:0.0}' would be one decimal place
             */
            GetComponent<Text>().text = String.Format("{0:0}", experience.GetPoints());
        }
    }
}

